#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <gio/gio.h>

static GDBusNodeInfo *introspection_data = NULL;

static const char *introspection_xml =
    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
    "<node name=\"/org/freedesktop/Notifications\">"
    "    <interface name=\"org.freedesktop.Notifications\">"

    "        <method name=\"Notify\">"
    "            <arg direction=\"in\"  name=\"app_name\"        type=\"s\"/>"
    "            <arg direction=\"in\"  name=\"replaces_id\"     type=\"u\"/>"
    "            <arg direction=\"in\"  name=\"app_icon\"        type=\"s\"/>"
    "            <arg direction=\"in\"  name=\"summary\"         type=\"s\"/>"
    "            <arg direction=\"in\"  name=\"body\"            type=\"s\"/>"
    "            <arg direction=\"in\"  name=\"actions\"         type=\"as\"/>"
    "            <arg direction=\"in\"  name=\"hints\"           type=\"a{sv}\"/>"
    "            <arg direction=\"in\"  name=\"expire_timeout\"  type=\"i\"/>"
    "            <arg direction=\"out\" name=\"id\"              type=\"u\"/>"
    "        </method>"

    "    </interface>"
    "</node>";
static void handle_method_call (GDBusConnection *connection, const gchar
		*sender, const gchar *object_path, const gchar *interface_name,
		const gchar *method_name, GVariant *parameters,
		GDBusMethodInvocation *invocation, gpointer user_data)
{
	if (!(g_strcmp0(method_name, "Notify"))) {
		GVariantType *input_type = g_variant_type_new("(susssasa{sv}i)");
		
		char *app_name;
		unsigned int replaces_id;
		char *app_icon, *summary, *body;
		char **actions;
		GVariant *hints;
		int expire_timeout;
		
		GVariantIter i;
		g_variant_iter_init(&i, parameters);
		g_variant_iter_next(&i, "s", &app_name);
		g_variant_iter_next(&i, "u", &replaces_id);
		g_variant_iter_next(&i, "s", &app_icon);
		g_variant_iter_next(&i, "s", &summary);
		g_variant_iter_next(&i, "s", &body);
		g_variant_iter_next(&i, "^a&s", &actions);
		g_variant_iter_next(&i, "@a{?*}", &hints);
		g_variant_iter_next(&i, "i", &expire_timeout);

		printf("Notification from %s:\nsummary: %s\nbody: %s\nexpires at: %d\n", app_name, summary, body, expire_timeout);
		g_dbus_method_invocation_return_value(invocation, g_variant_new("(u)", &replaces_id));
	}
}

static const GDBusInterfaceVTable interface_vtable = {handle_method_call, NULL, NULL};

static void bus_acquired_handler(GDBusConnection *connection, const gchar *name, gpointer user_data)
{
	GError *error;
	guint registration_id = g_dbus_connection_register_object(connection,
			"/org/freedesktop/Notifications",
			introspection_data->interfaces[0], &interface_vtable,
			NULL, NULL, &error);
	g_assert(registration_id > 0);
}

static void name_lost_handler()
{
	exit(1);
}

int main (int argc, char **argv)
{
	guint bus_name;
	introspection_data = g_dbus_node_info_new_for_xml(introspection_xml, NULL);

	bus_name = g_bus_own_name(G_BUS_TYPE_SESSION,
			"org.freedesktop.Notifications",
			G_BUS_NAME_OWNER_FLAGS_REPLACE, bus_acquired_handler,
			NULL, name_lost_handler, NULL, NULL);

	GMainLoop *main_loop = g_main_loop_new(NULL, FALSE);
	g_main_loop_run(main_loop);

	g_bus_unown_name(bus_name);
}
