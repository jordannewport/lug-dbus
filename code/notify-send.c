#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <systemd/sd-bus.h>

int main (int argc, char **argv)
{
	if (argc < 3) {
		fprintf(stderr, "Needs a header and body argument.\n");
		return EXIT_FAILURE;
	}
	/* setup */
	sd_bus_error error = SD_BUS_ERROR_NULL;
	sd_bus_message *message = NULL;
	sd_bus *bus = NULL;
	uint32_t message_id;
	int result;

	/* connect to session bus */
	result = sd_bus_open(&bus);
	if (result < 0) {
		fprintf(stderr, "Failed to connect to session bus: %s\n", strerror(-result));
		goto finish;
	}

	/* issue the method call and store the response in message */
	/* bus, service, object, interface, method, &error, &message, input signature, arguments */
	result = sd_bus_call_method(bus, "org.freedesktop.Notifications",
			"/org/freedesktop/Notifications",
			"org.freedesktop.Notifications", "Notify", &error,
			&message, "susssasa{sv}i", "notify-send", "", 0,
			argv[1], argv[2], 0, 0, 10000
			);
	if (result < 0) {
		printf("%d\n", result);
		fprintf(stderr, "Failed to issue method call: %s\n", strerror(-result));
		goto finish;
	}

	/* Parse response */
	result = sd_bus_message_read(message, "u", &message_id);
	if (result < 0) {
		fprintf(stderr, "Failed to parse response message: %s\n", strerror(-result));
		goto finish;
	}

	printf("Method returned notification ID %u\n", message_id);

finish:
	sd_bus_error_free(&error);
	sd_bus_message_unref(message);
	sd_bus_unref(bus);
	if (result < 0) return EXIT_FAILURE;
	else return EXIT_SUCCESS;
}
